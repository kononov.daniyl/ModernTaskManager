import  { createGlobalStyle } from "styled-components";
import { normalize } from 'styled-normalize'
import Theme from "@Theme";

/** @example @media screen and (max-width: 1023px)*/
export const Media = (width: number) => `@media screen and (max-width: ${width}px)`;

export const GeneralStyles =createGlobalStyle`
  ${normalize}

 @font-face {
    font-family: 'Montserrat';
    src: local('Montserrat'), local('Montserrat'),
      url('/fonts/Montserrat-Regular.ttf') format('truetype');
    font-weight: 400;
    font-style: normal;
  }

 @font-face {
    font-family: 'Montserrat';
    src: local('Montserrat'), local('Montserrat'),
      url('/fonts/Montserrat-Medium.ttf') format('truetype');
    font-weight: 500;
    font-style: normal;
  }

 @font-face {
    font-family: 'Montserrat';
    src: local('Montserrat'), local('Montserrat'),
      url('/fonts/Montserrat-SemiBold.ttf') format('truetype');
    font-weight: 600;
    font-style: normal;
  }

  * {
    box-sizing: border-box;
  }

  *:after, *:before {
    box-sizing: inherit;
  }

  body, html {
    height: 100%;
    margin: 0;
    padding: 0;
  }

  body {
    font-family: ${Theme.fonts.main}, ${Theme.fonts.second}, sans-serif;
    font-size: ${Theme.regularFontSize};
    color: ${Theme.colors.text};
    font-weight: ${Theme.fontWeight.regular};
    line-height: ${Theme.lineHeight};
    min-width: 320px;
    overflow-x: hidden;
    background: #fff;
  }

  h1, h2, h3, h4, h5, h6 {
    font-family: ${Theme.fonts.main}, ${Theme.fonts.second}, sans-serif;
    color: ${Theme.colors.text};
    font-weight: ${Theme.fontWeight.medium};
    line-height: ${Theme.lineHeight};
    margin: 0;
  }

  ul, li, ol {
    margin: 0;
    padding: 0;
    display: block;
  }

  p {
    margin: 0;
  }

  img {
    max-width: 100%;
    max-height: 100%;
    display: block;
  }

  a {
    text-decoration: none;
    color: ${Theme.colors.text};
    transition: ${Theme.transition};

    &:hover {
      text-decoration: none;
      color: ${Theme.colors.accent};
    }
  }

  input, button, a {
    outline: none;
  }

  .container {
    width: 100%;
    padding: 0 50px;
    margin: 0 auto;
    max-width: 1660px;
  }
`;
