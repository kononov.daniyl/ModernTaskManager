import React from 'react';
import { useRoutes } from 'src/front/src/Router';

export const App = () => {
  const routes = useRoutes();

  return (
    <div>
       {routes}
    </div>
  )
}