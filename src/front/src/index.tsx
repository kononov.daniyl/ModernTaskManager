import React from 'react';
import ReactDom from 'react-dom';
import { App } from './App';
import { GeneralStyles } from '../generalStyles';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { SnackbarProvider } from 'notistack';
import { store } from './redux';

ReactDom.render(
  <>
    <GeneralStyles />
    <SnackbarProvider>
      <Provider store={store}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </Provider>
    </SnackbarProvider>
  </>,
  document.getElementById('app')
);
