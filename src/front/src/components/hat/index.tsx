import React, { Dispatch } from 'react';
import styled from 'styled-components';
import { _INavElement } from '../../types';
import { Button } from '@Components/button';
import Theme from '@Theme';
import { connect, useSelector } from 'react-redux';
import { _IStore, _IActions } from '@Redux/storeParts';
import { useAuth } from '@Hooks/useAuth';
import { Link } from 'react-router-dom';

interface _StoreProps {
  isAuthenticated: _IStore['isAuthenticated'];
}

interface _IDispatchProps {
  dispatch: Dispatch<_IActions[
    'setActiveModalName'
  ]>
}

interface _IOwnProps {
  nav: _INavElement[];
}

export const Hat = connect<_StoreProps, _IDispatchProps, _IOwnProps, _IStore>(
  (s, p) => ({
    isAuthenticated: s.isAuthenticated
  })
)(
  (p: _StoreProps & _IDispatchProps & _IOwnProps) => {
    const {
      nav,
      isAuthenticated,
      dispatch
    } = p;

    const { logout } = useAuth();

    const handleLoginClick = () => {
      if (isAuthenticated) {
        logout();
      } else {
        dispatch({ type: 'setActiveModalName', value: 'authModal' });
      }
    }

    return (
      <Wrap>
        <div className="container">
          <Inner>
            {
              isAuthenticated ? (
                <Nav>
                  {
                    nav.map(({ text, link }) => (
                      <Link key={text}  to={link}>
                        <Button>
                          {text}
                        </Button>
                      </Link>
                    ))
                  }
                </Nav>
              ) : null
            }
            <StyledButton onClick={handleLoginClick} >
              {
                isAuthenticated ? 'Выйти' : 'Войти'
              }
            </StyledButton>
          </Inner>
        </div>
      </Wrap>
    )
  }
);

const Wrap = styled.div`
  background: ${Theme.colors.darkBg};
  padding: 20px 0;
  margin-bottom: 50px;
`;

const Inner = styled.div`
  display: flex;
  align-items: center;
`;

const Nav = styled.nav`
  display: flex;
  align-items: center;
`;

const StyledButton = styled(Button)`
  margin-left: auto;
`;