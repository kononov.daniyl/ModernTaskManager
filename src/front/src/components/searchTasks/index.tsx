import React from 'react';
import { Input } from '@Components/input';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import { getSearchQuery } from '@Selectors';
import { _IActions } from '@Redux/storeParts';
import MagnifierIcon from '@Images/icons/magnifier.svg';
import { IconContainer } from '@Components/iconContainer';

export const SearchTasks = () => {
  const searchQuery = useSelector(getSearchQuery);
  const dispatch = useDispatch();

  const handleChange = (value: string) =>{
    dispatch<_IActions['setSearchQuery']>({type:'setSearchQuery', value});
  }

  const handleSearch = (e:React.FormEvent<HTMLFormElement>) =>{
    e.preventDefault();

    dispatch<_IActions['sendSearchQuery']>({type:'sendSearchQuery', value: null});
  }

  return(
   <Form onSubmit={handleSearch}>
      <Search
        type='text'
        label='Поиск задачи'
        onChange={handleChange}
        value={searchQuery}
      />
      <Button><IconContainer><MagnifierIcon/></IconContainer></Button>
   </Form>
  )
}

const Search = styled(Input)`
  margin-top: 20px;

  .input-label{
    font-size: 20px;
  }
`;

const Form = styled.form`
  position: relative;
`;

const Button = styled.button`
  background: none;
  border: none;
  padding: 0;
  position: absolute;
  right: 5px;
  bottom: 3px;
`;