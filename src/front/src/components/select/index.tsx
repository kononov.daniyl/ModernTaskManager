import React from 'react';
import styled from 'styled-components';
import { Select as MaterialSelect, MenuItem, InputLabel } from '@material-ui/core';
import { _IOption } from '../../types';

interface _ISelect extends _IOption {
  items: _IOption[];
  onChange(value: string): void;
}

export const Select = (p: _ISelect) => {
  const {
    label,
    value,
    items,
    onChange
  } = p;

  const handleChange = ({ target: { value } }: React.ChangeEvent<{ value: unknown  }>) => {
    onChange(value as string);
  }

  return (
    <Label>
      <Text>
        {label}
      </Text>
      <StyledSelect
        value={value}
        onChange={handleChange}
      >
        {
          items.map(({label, value}) => (
            <MenuItem value={value} key={value}>{label}</MenuItem>
          ))
        }
      </StyledSelect>
    </Label>
  )
}

const Label = styled(InputLabel)`
  display: block;
  cursor: text;
  width: 100%;

  &:not(:last-of-type){
    margin-bottom: 15px;
  }
`;

const Text = styled.div`
  font-size: 12px;
`;

const StyledSelect = styled(MaterialSelect)`
  width: 100%;
`;