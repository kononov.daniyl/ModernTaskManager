import React, { useState, useCallback, useEffect } from "react";
import { Task } from "@Components/task";
import { _IStatusOption, _ITaskList } from "../../types";
import { useHttp } from "@Hooks/useHttp";
import { useSelector, useDispatch } from "react-redux";
import { _IStore, _IActions } from "@Redux/storeParts";
import styled from "styled-components";
import Theme from "@Theme";
import { getToken, getTasks } from "@Selectors";

interface _ITaskListComponent{
  statusVariants: _IStatusOption[];
}

export const TaskList = (p:_ITaskListComponent) =>{
  const {
    statusVariants
  } = p;
  const { request, isLoading } = useHttp();
  const token = useSelector(getToken);
  const tasks= useSelector(getTasks);
  const dispatch = useDispatch();

  const fetchedTasks = useCallback(async () => {
    try {
      const tasks: _ITaskList = await request({
        url: '/api/task',
        method: 'GET',
        headers: { Authorization: `Bearer ${token}` }
      });

      dispatch<_IActions['setTasks']>({type: 'setTasks', value: tasks});
    } catch (e) {

    }
  }, [token, request]);

  useEffect(() => {
    fetchedTasks()
  }, [fetchedTasks]);

  return (
    <AllTasks>
    {
      isLoading ? (
        <div>Loading...</div>
      ) : tasks.length === 0 ? (
        <div>Not found any tasks</div>
      ) : (
        tasks.map((task) => (
          <Task {...task} key={task._id} statusVariants={statusVariants} />
        ))
      )
    }
    </AllTasks>
  );
}

const AllTasks = styled.div`
  width: 800px;
  border: 2px solid ${Theme.colors.darkBg};
  border-radius: 5px;
  padding: 15px;
`;