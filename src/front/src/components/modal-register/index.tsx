import React, { useState, useEffect } from 'react';
import { AuthModalTab, _IAuthStateValue, _IUserAuth } from '../../types';
import styled from 'styled-components';
import { Button } from '@Components/button';
import Theme from '@Theme';
import { Input } from '@Components/input';
import { useHttp } from '@Hooks/useHttp';
import { useSnackbar } from '@Hooks/useSnackbar';
import { useDispatch } from 'react-redux';
import { _IActions } from '@Redux/storeParts';
import { useAuth } from '@Hooks/useAuth';

export const RegisterModal = () => {
  const [activeTab, setActiveTab] = useState<AuthModalTab>('login');
  const [formData, setFormData] = useState<_IAuthStateValue>({
    email: '',
    password: '',
    confirmPassword: '',
  });

  const dispatch = useDispatch();

  const {
    isLoading,
    error,
    request,
    clearError
  } = useHttp();

  const {
    login
  } = useAuth();

  const {
    showSnackbar,
    showError
  } = useSnackbar();

  useEffect(() => {
    if (error) {
      showError(error);
    }
  }, [error]);

  const handleSetActiveTab = (tab: AuthModalTab) => {
    clearError();
    setActiveTab(tab);
  }

  const updateFormData = (field: keyof _IAuthStateValue) => (value: string) => {
    clearError();
    setFormData({
      ...formData,
      [field]: value
    });
  }

  const registerHandler = async () => {
    try {
      const data = await request<{ message: string }>({
        url: '/api/auth/register',
        method: 'POST',
        body: JSON.stringify(formData),
        headers: { 'Content-Type': 'application/json' }
      });

      handleSetActiveTab('login');
      showSnackbar({
        message: data.message,
        variant: 'success'
      });
    } catch { }
  }

  const loginHandler = async () => {
    try {
      const data = await request<_IUserAuth>({
        url: '/api/auth/login',
        method: 'POST',
        body: JSON.stringify(formData),
        headers: { 'Content-Type': 'application/json' }
      });

      login(data.token, data.userId);
      dispatch<_IActions['setActiveModalName']>({ type: 'setActiveModalName', value: null });
    } catch { }
  }

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (activeTab === 'register') {
      registerHandler();
    } else {
      loginHandler();
    }
  }

  return (
    <Wrap>
      <Title>
        {activeTab === 'login' ? 'Вход' : 'Регистрация'}
      </Title>
      <form onSubmit={handleSubmit} id='auth-form'>
        <Input
          label='Емейл'
          type="text"
          value={formData.email}
          onChange={updateFormData('email')}
        />
        <Input
          label='Пароль'
          type="password"
          value={formData.password}
          onChange={updateFormData('password')}
        />
        {
          activeTab === 'register' ? (
            <Input
              label='Подтвердите пароль'
              type="password"
              value={formData.confirmPassword}
              onChange={updateFormData('confirmPassword')}
            />
          ) : null
        }
        <SubmitButtonWrap>
          <SubmitButton isDisabled={isLoading}> Подтвердить </SubmitButton>
        </SubmitButtonWrap>
      </form>
      <Buttons>
        <StyledButton
          isActive={activeTab === 'login'}
          onClick={() => handleSetActiveTab('login')}
          isDisabled={isLoading}
        >
          Вход
        </StyledButton>
        <StyledButton
          isActive={activeTab === 'register'}
          onClick={() => handleSetActiveTab('register')}
          isDisabled={isLoading}
        >
          Регистрация
        </StyledButton>
      </Buttons>
    </Wrap>
  )
}

const Wrap = styled.div`
  width: 400px;
`;

const Title = styled.h3`
  text-align: center;
  margin-bottom: 20px;
`;

const Buttons = styled.div`
  display: flex;
  align-items: center;
  margin-top: 20px;
`;

const StyledButton = styled(Button) <{ isActive: boolean }>`
  padding: 8px 12px;
  color: ${({ isActive }) => isActive ? Theme.colors.interactiveHover : Theme.colors.darkBg};
  background: ${({ isActive }) => isActive ? Theme.colors.darkBg : '#fff'};
  border-radius: 3px;

  &:not(:last-of-type){
    margin-right: 10px;
  }

  &:active{
    color: ${({ isActive }) => isActive ? Theme.colors.interactiveHover : Theme.colors.darkBg};
  }
`;

const SubmitButton = styled(Button)`
  padding: 8px 12px;
  color: ${Theme.colors.darkBg};
  border-radius: 3px;
  background: ${Theme.colors.interactiveBg};

  &:hover{
    background: ${Theme.colors.darkBg};
  }
`;

const SubmitButtonWrap = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

