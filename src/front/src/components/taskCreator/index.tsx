import React, { useState, useEffect } from "react";
import { _IStatusOption, _ITask, _ITaskWithId } from "../../types";
import { useHttp } from "@Hooks/useHttp";
import { useSnackbar } from "@Hooks/useSnackbar";
import { useSelector, useDispatch } from "react-redux";
import { _IStore, _IActions } from "@Redux/storeParts";
import { Input } from '@Components/input';
import { Select } from '@Components/select';
import styled from "styled-components";
import Theme from "@Theme";
import { Button } from "@Components/button";
import { getToken } from "@Selectors";

interface _ITaskCreator {
  statusVariants: _IStatusOption[];
}

const initialTask: _ITask = {
  description: '',
  status: 'expectation',
  title: '',
};

export const TaskCreator = (p: _ITaskCreator) => {
  const {
    statusVariants
  } = p;
  const { request, isLoading, error } = useHttp();
  const { showError, showSnackbar } = useSnackbar();
  const token = useSelector(getToken);
  const [formData, setFromData] = useState<_ITask>(initialTask);
  const dispatch = useDispatch();

  useEffect(() => {
    if (error) {
      showError(error);
    }
  }, [error]);

  const handleChange = (prop: keyof _ITask) => (value: string) => {
    setFromData({
      ...formData,
      [prop]: value
    })
  }

  const handleCreateTask = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      const data: {task: _ITaskWithId} = await request({
        url: '/api/task/add',
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        }
      });

      showSnackbar({
        message: `Task '${data.task.title}' is created`,
        variant: 'success'
      });
      dispatch<_IActions['createTask']>({type:'createTask', value: null});
      setFromData(initialTask);
    } catch (e) {

    }
  }

  return (
    <Form onSubmit={handleCreateTask}>
      <Input
        onChange={handleChange('title')}
        label='Заголовок'
        type='text'
        value={formData.title}
      />
      <Select
        label='Статус'
        value={formData.status}
        items={statusVariants}
        onChange={handleChange('status')}
      />
      <Input
        onChange={handleChange('description')}
        label='Описание'
        type='text'
        value={formData.description}
      />
      <StyledButton isDisabled={isLoading}>Создать</StyledButton>
    </Form>
  )
}

const Form = styled.form`
  width: 500px;
  border: 2px solid ${Theme.colors.darkBg};
  border-radius: 5px;
  padding: 15px;

  &:not(:last-of-type){
    margin-bottom: 15px;
  }
`;

const StyledButton = styled(Button)`
  padding: 8px 12px;
  color: ${Theme.colors.interactive};
  background: ${Theme.colors.darkBg};
  border-radius: 3px;
  margin-top: 15px;

  &:active{
    color: ${Theme.colors.interactiveHover};
  }
`;