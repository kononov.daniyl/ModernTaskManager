import React, { Dispatch } from 'react';
import styled from 'styled-components';
import { AnyContent } from '../../types';
import { connect } from 'react-redux';
import { _IStore, _IActions } from '@Redux/storeParts';

interface _StoreProps{
  activeModalName: _IStore['activeModalName'];
}

interface _IDispatchProps{
  dispatch:Dispatch<_IActions[
    'setActiveModalName'
  ]>;
}

interface _IOwnProps{
  children: AnyContent;
}

export const Modal = connect<_StoreProps,_IDispatchProps, _IOwnProps, _IStore>(
  (s,p)=>({
    activeModalName: s.activeModalName
  })
)(
  (p:_StoreProps & _IDispatchProps &  _IOwnProps)=>{
    const {
      children,
      activeModalName,
      dispatch
     } = p;

    if(activeModalName === null) return null;

    const handleCloseModal = () =>{
      dispatch({type: 'setActiveModalName', value: null});
    }

    return (
      <ModalWrap>
        <Overlay onClick={handleCloseModal} />
        <Content>
          {children}
        </Content>
      </ModalWrap>
    );
  }
)

const ModalWrap = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  cursor: pointer;
  background: #00000050;
  z-index: 0;
`;

const Content = styled.div`
  padding: 15px;
  background: #fff;
  border-radius: 5px;
  z-index: 1;
  position: relative;
`;