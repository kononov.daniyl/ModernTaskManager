import React, { useState, useEffect } from "react";
import { _IStatusOption, _ITask, _ITaskWithId } from "../../types";
import { useHttp } from "@Hooks/useHttp";
import { useSnackbar } from "@Hooks/useSnackbar";
import { useSelector, useDispatch } from "react-redux";
import { _IStore, _IActions } from "@Redux/storeParts";
import { Input } from '@Components/input';
import { Select } from '@Components/select';
import styled from "styled-components";
import Theme from "@Theme";
import { Button } from "@Components/button";
import { getToken, getEditedTask } from "@Selectors";

interface _ITaskEditor {
  statusVariants: _IStatusOption[];
}

export const TaskEditor = (p: _ITaskEditor) => {
  const {
    statusVariants,
  } = p;

  const task = useSelector(getEditedTask);

  const [formData, setFromData] = useState<_ITaskWithId>(task!);

  const token = useSelector(getToken);
  const dispatch = useDispatch();

  const { request, isLoading, error } = useHttp();
  const { showError, showSnackbar } = useSnackbar();

  useEffect(() => {
    if (error) {
      showError(error);
    }
  }, [error]);

  const isInitialTask = task._id === '';

  useEffect(()=>{
    setFromData(task);
  }, [task])

  if (isInitialTask) {
    return null;
  }

  const handleChange = (prop: keyof _ITask) => (value: string) => {
    setFromData({
      ...formData,
      [prop]: value
    })
  }

  const handleEditTask = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      const data: { message: string } = await request({
        url: '/api/task/edit',
        method: 'PATCH',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        }
      });

      showSnackbar({
        message: data.message,
        variant: 'success'
      });
      dispatch<_IActions['updateTask']>({ type: 'updateTask', value: null });
      dispatch<_IActions['resetEditedTask']>({type:'resetEditedTask', value: null});
    } catch (e) {

    }
  }

  return (
    <Form onSubmit={handleEditTask}>
      <Input
        onChange={handleChange('title')}
        label='Заголовок'
        type='text'
        value={formData.title}
      />
      <Select
        label='Статус'
        value={formData.status}
        items={statusVariants}
        onChange={handleChange('status')}
      />
      <Input
        onChange={handleChange('description')}
        label='Описание'
        type='text'
        value={formData.description}
      />
      <StyledButton isDisabled={isLoading}>Сохранить</StyledButton>
    </Form>
  )
}

const Form = styled.form`
  width: 500px;
  border: 2px solid ${Theme.colors.darkBg};
  border-radius: 5px;
  padding: 15px;

  &:not(:last-of-type){
    margin-bottom: 15px;
  }
`;

const StyledButton = styled(Button)`
  padding: 8px 12px;
  color: ${Theme.colors.interactive};
  background: ${Theme.colors.darkBg};
  border-radius: 3px;
  margin-top: 15px;

  &:active{
    color: ${Theme.colors.interactiveHover};
  }
`;