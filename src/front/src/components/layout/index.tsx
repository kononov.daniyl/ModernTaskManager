import React from 'react';
import { Hat } from "@Components/hat"
import { _INavElement } from '../../types';

interface _ILayput {
  children?: JSX.Element | JSX.Element[] | string | number;
}

const hatNav: _INavElement[] = [{
  text: 'Задачи',
  link: '/tasks',
}, {
  text: 'О нас',
  link: '/about'
}];

export const Layout = (p: _ILayput) => {
  const {
    children
  } = p;

  return (
    <>
      <Hat nav={hatNav} />
      {children}
    </>
  )
}