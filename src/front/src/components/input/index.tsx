import React from 'react';
import styled from 'styled-components';
import { Input as MaterialInput,InputLabel  } from '@material-ui/core';
import { _IOption } from '../../types';

interface _IInput extends _IOption {
  type: string;
  onChange(value: string): void;
  className?: string;
}

export const Input = (p: _IInput) => {
  const {
    label,
    value,
    type,
    onChange,
    className
  } = p;

  const handleChange = ({target:{value}}:React.ChangeEvent<HTMLInputElement>)=>{
    onChange(value);
  }

  return (
    <Label className={className}>
      <Text className='input-label'>
        {label}
      </Text>
      <Inp value={value} type={type} onChange={handleChange} />
    </Label>
  )
}

const Label = styled(InputLabel)`
  display: block;
  cursor: text;
  width: 100%;

  &:not(:last-of-type){
    margin-bottom: 15px;
  }
`;

const Text = styled.div`
  font-size: 12px;
`;

const Inp = styled(MaterialInput)`
  width: 100%;
`;