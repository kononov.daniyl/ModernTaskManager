import React from "react";
import styled from "styled-components";
import Theme from "@Theme";

export const IconContainer = ({children}: {children: JSX.Element}) =>(
    <Icon>
      {children}
    </Icon>
  );

const Icon = styled.div`
height: 25px;

&:not(:last-of-type){
  margin-right: 15px;
}

svg{
  height: 25px;
  width: auto;
  fill: ${Theme.colors.darkBg};
  cursor: pointer;
  transition: ${Theme.transition};

  &:hover{
    border-color: ${Theme.colors.accent};
    fill: ${Theme.colors.accent};
  }
}
`;