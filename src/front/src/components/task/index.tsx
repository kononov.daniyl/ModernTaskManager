import React, { useEffect } from 'react';
import { _ITaskWithId, _IStatusOption } from "../../types";
import styled from "styled-components";
import Theme from '@Theme';
import { useHistory } from 'react-router-dom';
import { useHttp } from '@Hooks/useHttp';
import { useSelector, useDispatch } from 'react-redux';
import { getToken } from '@Selectors';
import { useSnackbar } from '@Hooks/useSnackbar';
import { _IActions } from '@Redux/storeParts';
import RemoveIcon from '@Images/icons/remove.svg';
import EditIcon from '@Images/icons/edit.svg';
import ViewIcon from '@Images/icons/view.svg';
import { IconContainer } from '@Components/iconContainer';

export const Task = (p: _ITaskWithId & { statusVariants: _IStatusOption[] }) => {
  const {
    description,
    status,
    title,
    _id,
    statusVariants
  } = p;

  const {request, isLoading, error} =  useHttp();
  const {showSnackbar, showError} = useSnackbar();
  const token = useSelector(getToken);
  const dispatch = useDispatch();
  const satusLabel = statusVariants.find(({ value }) => value === status)?.label;
  const history = useHistory();

  useEffect(() => {
    if (error) {
      showError(error);
    }
  }, [error]);

  const handleEdit = () => {
    dispatch<_IActions['setEditedTask']>({type:'setEditedTask', value: {description, status, title, _id}});
  };

  const handleInto = () => {
    history.push(`/task/${_id}`);
  };

  const handleRemove = async ()=>{
    try {
      const data = await request<{ message: string }>({
        url: '/api/task/delete',
        method: 'DELETE',
        body: JSON.stringify({_id}),
        headers:{
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json'
         }
      });
      showSnackbar({
        message: data.message,
        variant: 'success'
      });
      dispatch<_IActions['deleteTask']>({type:'deleteTask', value: null});
    } catch (e) {

    }
  }

  const borderColor = status === 'done'
    ? Theme.colors.success
    : status === 'in progress'
      ? Theme.colors.warning
      : status === 'expectation'
        ? Theme.colors.darkBg
        : Theme.colors.danger;

  return (
    <Wrap borderColor={borderColor} isLoading={isLoading}>
      <Text>
        <Title>
          {title}
        </Title>
        <Status>
          {satusLabel}
        </Status>
      </Text>
      <Desc>
        {description}
      </Desc>
      <Controls className='task-control'>
        <IconContainer><ViewIcon onClick={handleInto}/></IconContainer>
        <IconContainer><EditIcon onClick={handleEdit}/></IconContainer>
        <IconContainer><RemoveIcon onClick={handleRemove}/></IconContainer>
      </Controls>
    </Wrap>
  );
}

const Wrap = styled.div<{ borderColor: string; isLoading: boolean }>`
  transition: ${Theme.transition};
  cursor: pointer;
  padding: 5px;
  border-top-right-radius: 3px;
  border-top-left-radius: 3px;
  border-bottom: 2px solid ${({ borderColor }) => borderColor};
  position: relative;
  opacity: ${({isLoading})=>isLoading ? 0.6 : 1};

  &:not(:last-of-type){
    margin-bottom: 10px;
  }

  &:hover{
    background: #00000015;

    .task-control{
      opacity: 1;
    }
  }
`;

const Text = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Status = styled.div`
  font-size: 16px;
`;

const Title = styled.h2`
  text-transform: uppercase;
  font-size: 18px;
`;

const Desc = styled.p`
  font-size: 14px;
`;

const Controls = styled.div`
  position: absolute;
  top: 5px;
  right: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #fff;
  padding: 5px;
  transition: ${Theme.transition};
  opacity: 0;
  border-radius: 3px;
`;