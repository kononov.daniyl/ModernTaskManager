import React from 'react';
import { Switch, Route, Redirect } from "react-router-dom"
import { HomePage } from "@Pages/home"
import { AuthPage } from "@Pages/auth"
import { useSelector } from 'react-redux';
import { _IStore } from '@Redux/storeParts';
import { Layout } from '@Components/layout';
import { TaskPage } from '@Pages/task';
import { getIsAuthenticated } from './Selectors';

export const useRoutes = () => {
  const isAuthenticated = useSelector(getIsAuthenticated);

  if (isAuthenticated) {

    return (
      <Switch>
        <Route path='/' exact>
          <HomePage />
        </Route>
        <Route path='/about' exact>
          <Layout>
            <div>
              about
          </div>
          </Layout>
        </Route>
        <Route path='/task/:id'>
          <TaskPage/>
        </Route>
        <Redirect to='/' />
      </Switch>
    )
  }

  return (
    <Switch>
      <Route path='/auth' exact>
        <AuthPage />
      </Route>
      <Redirect to='/auth' />
    </Switch>
  )
}