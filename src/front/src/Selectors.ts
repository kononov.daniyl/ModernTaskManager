import { _IStore } from "@Redux/storeParts"

export const getIsAuthenticated = (s: _IStore)=>s.isAuthenticated;
export const getToken = (s: _IStore)=>s.token;
export const getTasks = (s: _IStore) => s.tasks;
export const getEditedTask = (s: _IStore) => s.editedTask;
export const getSearchQuery = (s: _IStore) => s.searchQuery;
export const getTaskById = (id: _IStore['tasks'][0]['_id']) => (s: _IStore) => s.tasks.find((task)=>task._id === id);

