import { useCallback, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { _IStore, _IActionTypes, _IActions } from "@Redux/storeParts";
import { _IUserAuth } from "../types";
import { useSnackbar } from "./useSnackbar";

const storageField = 'user';

export const useAuth = () =>{
  const {token, userId} = useSelector((s: _IStore)=>({
    token: s.token,
    userId: s.userId
  }));

  const {showSnackbar} = useSnackbar();

  const dispatch = useDispatch();

  const login = useCallback((jwtToken: _IUserAuth['token'], id: _IUserAuth['userId'])=>{
    dispatch<_IActions['setToken']>({type: 'setToken', value: jwtToken});
    dispatch<_IActions['setUserId']>({type: 'setUserId', value: id});
    dispatch<_IActions['setIsAuthenticated']>({type: 'setIsAuthenticated', value: true});

    localStorage.setItem(storageField, JSON.stringify({userId: id, token: jwtToken}));

    showSnackbar({
      message: 'Login success',
      variant: 'info'
    });
  }, []);

  const logout = useCallback(()=>{
    dispatch({type: 'setToken', value: ''});
    dispatch({type: 'userId', value: ''});
    dispatch<_IActions['setIsAuthenticated']>({type: 'setIsAuthenticated', value: false});

    localStorage.removeItem(storageField);
    showSnackbar({
      message: 'Logout success',
      variant: 'info'
    });
  }, []);

  useEffect(()=>{
    const storageUser = localStorage.getItem(storageField);

    const data: _IUserAuth = JSON.parse(storageUser === null ? '{}' : storageUser);

    if(data && data.token && !token){
      login(data.token, data.userId);
    }
  }, [login]);

  return {
    login,
    logout,
    token,
    userId
  };
}