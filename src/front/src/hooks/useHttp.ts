import { useState, useCallback } from "react";
import { _IResponseBody, _IUseHttpError } from "../types";

export const useHttp = () => {
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState<_IUseHttpError | null>(null);

  const request = useCallback(async function<T>({
    url,
    method = 'GET',
    body = null,
    headers = {}
  }: {
    url: string,
    method: "GET" | "POST" | "DELETE" | "PATCH",
    body?: any,
    headers: any
  }){
    setLoading(true);

    try {
      const resp = await fetch(url, { method, body, headers });
      const data:T | _IUseHttpError = await resp.json();
      setLoading(false);

      if(!resp.ok){
        setError(data as _IUseHttpError);
        throw new Error('Something went wrong');
      }

      return data as T;
    } catch (e) {
      setLoading(false);
      if(e.message !== 'Something went wrong'){
        setError({ message: e.message });
      }
      throw e;
    }
  }, []);

  const clearError = () => setError(null);

  return { isLoading, error, request, clearError }
}