import React from 'react';
import { useSnackbar as useSnackbarN } from 'notistack'
import { useCallback } from 'react';
import { SackbarVariants, _IUseHttpError } from '../types';
import Theme from '@Theme';
import styled from 'styled-components';

interface _ISnackbar {
  message: string;
  jsx?: JSX.Element;
  variant?: SackbarVariants
}

export const useSnackbar = ()=>{
  const { enqueueSnackbar } = useSnackbarN();

  const showSnackbar = useCallback(({message, jsx, variant}:_ISnackbar)=>{
    return enqueueSnackbar(message, {
      action: jsx,
      variant,
      autoHideDuration: 2000,
    });
  },[]);

  const showError = useCallback((error: _IUseHttpError)=>{
    return enqueueSnackbar(error.message, {
      action: (
        <div>
        {
          error.errors && error.errors.map(({ msg }, i) => (
            <ExactErrorText key={i}>
              {msg}
            </ExactErrorText>
          ))
        }
      </div>
      ),
      variant: 'error',
      autoHideDuration: 2000,
    });
  },[]);

  return {
    showSnackbar,
    showError
  }
}

const ExactErrorText = styled.div`
  border-bottom: 1px solid ${Theme.colors.darkBg};
  font-size: 16px;

  &:not(:last-of-type){
    margin-bottom: 10px;
  }
`;