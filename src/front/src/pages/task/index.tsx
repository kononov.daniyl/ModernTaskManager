import React, { useState, useEffect, useCallback } from "react";
import { Layout } from "@Components/layout";
import { useParams } from "react-router-dom";
import { _ITask, _ITaskWithId } from "../../types";
import { useSelector } from "react-redux";
import { getTaskById, getToken } from "@Selectors";
import { useHttp } from "@Hooks/useHttp";
import styled from "styled-components";

export const TaskPage = () => {
  const { id } = useParams();

  const thisTaskInRedux = useSelector(getTaskById(id));
  const token = useSelector(getToken);

  const initialTaskData: _ITask = {
    ...{
      description: '',
      status: 'expectation',
      title: ''
    },
    ...thisTaskInRedux
  };

  console.log(initialTaskData);

  const { request } = useHttp();

  const [task, setTask] = useState<_ITask>(initialTaskData);

  const {
    title,
    description,
    status
  } = task;

  const fetchedTask = useCallback(async () => {
    const task: _ITaskWithId = await request({
      url: `/api/task/${id}`,
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
    });

    setTask(task);
  }, [id]);

  useEffect(() => {
    fetchedTask();
  }, [fetchedTask]);

  return (
    <Layout>
      <div className='container'>
        <Inner>
          <TextRow>
            <RowName>
              Загловок
            </RowName>
            <Title>
              {title}
            </Title>
          </TextRow>
          <TextRow>
            <RowName>
              Описание
            </RowName>
            <Description>
              {description}
            </Description>
          </TextRow>
          <TextRow>
            <RowName>
              Статус
            </RowName>
            <Status>
              {status}
            </Status>
          </TextRow>
        </Inner>
      </div>
    </Layout>
  );
}


const Inner = styled.div`

`;

const Title = styled.h1`

`;

const Description = styled.h2`

`;

const Status = styled.p`

`;

const RowName = styled.span`
  width: 120px;
  font-size: 14px;
`;

const TextRow = styled.div`
  display: flex;
  align-items: flex-end;
`;