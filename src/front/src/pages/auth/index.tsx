import React, { useState } from 'react';
import { Layout } from '@Components/layout';
import styled from 'styled-components';
import { Modal } from '@Components/modal';
import { RegisterModal } from '@Components/modal-register';

export const AuthPage = () => {

  return (
    <Layout>
      <div className="container">
        <TitleWrap>
          <Title>
            Войдите что бы продолжить
          </Title>
          <p>
            Тут может быть ваша реклама
          </p>
        </TitleWrap>
      </div>
      <Modal>
        <RegisterModal />
      </Modal>
    </Layout>
  )
}

const TitleWrap = styled.div`
  padding-left: 50%;
`;

const Title = styled.h1`
`;

