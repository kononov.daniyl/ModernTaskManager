import React from 'react';
import { Layout } from '@Components/layout';
import styled from 'styled-components';

import { _ITask, _ITaskList, _IStatusOption } from '../../types';
import { _IStore } from '@Redux/storeParts';
import { TaskCreator } from '@Components/taskCreator';
import { TaskList } from '@Components/taskList';
import { TaskEditor } from '@Components/taskEditor';
import { SearchTasks } from '@Components/searchTasks';

const statusVariants: _IStatusOption[] = [{
  label: 'Готово',
  value: 'done'
}, {
  label: 'В процессе',
  value: 'in progress'
}, {
  label: 'В ожидании',
  value: 'expectation'
}, {
  label: 'Заблокировано',
  value: 'blocked'
}];

export const HomePage = () => {

  return (
    <Layout>
      <div className="container">
        <Inner>
          <Title>Задачи</Title>
          <SearchTasks />
          <ContentWrap>
              <TaskList statusVariants={statusVariants} />
              <div>
                <TaskCreator statusVariants={statusVariants}  />
                <TaskEditor statusVariants={statusVariants} />
              </div>
          </ContentWrap>
        </Inner>
      </div>
    </Layout >
  )
}

const Title = styled.h1`

`;

const Inner = styled.div`

`;

const ContentWrap = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 30px;
  align-items: flex-start;
`;

