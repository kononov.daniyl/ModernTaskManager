import { _IMongoUser } from "src/helpers/types";

export type AnyContent = JSX.Element | JSX.Element[] | string | number;

export interface _INavElement{
  text: string;
  link: string;
}

export type AuthModalTab = 'login' | 'register';

export interface _IAuthStateValue {
  email: _IMongoUser['email'];
  password: _IMongoUser['password'];
  confirmPassword: _IMongoUser['password'];
}

export interface _IResponseBody{
  message: string;
}

export interface _IUseHttpError{
  message: string;
  errors?: Array<{
    msg: string
  }>;
}

export type SackbarVariants = "default" | "error" | "success" | "warning" | "info";

export interface _IUserAuth {
  token: string;
  userId: string;
}

export interface _ITask{
  title: string;
  description: string;
  status: 'done' | 'in progress' | 'expectation' | 'blocked';
}

export interface _ITaskWithId extends _ITask{
  _id: string
}

export type _ITaskList = _ITaskWithId[];

export interface _IOption {
  label: string;
  value: string;
}

export interface _IStatusOption{
  label: string;
  value: _ITask['status'];
}