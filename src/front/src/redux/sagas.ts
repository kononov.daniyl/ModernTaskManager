import { takeEvery, all } from 'redux-saga/effects'
import { _IActions } from './storeParts';
import { fetchTasks } from './storeParts/tasks';

function* rootSata() {
  yield all([
    takeEvery<_IActions['createTask']>('createTask', fetchTasks),
    takeEvery<_IActions['deleteTask']>('deleteTask', fetchTasks),
    takeEvery<_IActions['updateTask']>('updateTask', fetchTasks),
    takeEvery<_IActions['sendSearchQuery']>('sendSearchQuery', fetchTasks),
  ]);
}

export {rootSata};