import { _IGenericAction } from '../types';
import { _IStore, AllActions } from '.';

declare module './index' {
  interface _IStore {
    userId: string;
  }

  interface _IActionTypes {
    setUserId: 'setUserId';
  }

  interface _IActions {
    setUserId: _IGenericAction<_IActionTypes['setUserId'], string>;
  }
}

export const userId = function userId( state: _IStore['userId'] = '', action: AllActions ) {
  switch( action.type ) {
    case 'setUserId':
      return action.value;
    default:
      return state;
  }
};
