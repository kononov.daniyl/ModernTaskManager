import { _IGenericAction } from '../types';
import { _IStore, AllActions } from '.';

type AllModals = 'authModal' | null;

declare module './index' {
  interface _IStore {
    activeModalName: AllModals;
  }

  interface _IActionTypes {
    setActiveModalName: 'setActiveModalName';
  }

  interface _IActions {
    setActiveModalName: _IGenericAction<_IActionTypes['setActiveModalName'], AllModals>;
  }
}

export const activeModalName = function activeModalName( state: _IStore['activeModalName'] = null, action: AllActions ) {
  switch( action.type ) {
    case 'setActiveModalName':
      return action.value;
    default:
      return state;
  }
};
