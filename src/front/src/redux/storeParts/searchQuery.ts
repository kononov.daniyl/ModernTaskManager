import { _IGenericAction } from '../types';
import { _IStore, AllActions } from '.';

declare module './index' {
  interface _IStore {
    searchQuery: string;
  }

  interface _IActionTypes {
    setSearchQuery: 'setSearchQuery';
    sendSearchQuery: 'sendSearchQuery';
  }

  interface _IActions {
    setSearchQuery: _IGenericAction<_IActionTypes['setSearchQuery'], string>;
    sendSearchQuery: _IGenericAction<_IActionTypes['sendSearchQuery'], null>;
  }
}

export const searchQuery = function searchQuery( state: _IStore['searchQuery'] = '', action: AllActions ) {
  switch( action.type ) {
    case 'setSearchQuery':
      return action.value;
    default:
      return state;
  }
};
