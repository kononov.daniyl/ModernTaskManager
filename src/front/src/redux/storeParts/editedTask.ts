import { _IGenericAction } from '../types';
import { _IStore, AllActions } from '.';
import { _ITaskWithId } from '../../types';

declare module './index' {
  interface _IStore {
    editedTask: _ITaskWithId;
  }

  interface _IActionTypes {
    setEditedTask: 'setEditedTask';
    resetEditedTask: 'resetEditedTask';
  }

  interface _IActions {
    setEditedTask: _IGenericAction<_IActionTypes['setEditedTask'], _ITaskWithId>;
    resetEditedTask: _IGenericAction<_IActionTypes['resetEditedTask'], null>;
  }
}

const initialTask: _ITaskWithId = {
  _id: '',
  title: '',
  description: '',
  status: 'expectation'
}

export const editedTask = function editedTask( state: _IStore['editedTask'] = initialTask, action: AllActions ) {
  switch( action.type ) {
    case 'setEditedTask':
      return action.value;
    case 'resetEditedTask':
      return initialTask;
    default:
      return state;
  }
};

