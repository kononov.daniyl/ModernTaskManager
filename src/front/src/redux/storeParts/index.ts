import { combineReducers } from 'redux';
import { activeModalName } from './activeModalName';
import { userId } from './userId';
import { token } from './token';
import { isAuthenticated } from './isAuthenticated';
import { tasks } from './tasks';
import { editedTask } from './editedTask';
import { searchQuery } from './searchQuery';

export interface _IStore {}
export interface _IActionTypes {}
export interface _IActions {}

export type AllActions = _IActions[keyof _IActions];

export const rootReducer = combineReducers<_IStore, AllActions>( {
  activeModalName,
  userId,
  token,
  isAuthenticated,
  tasks,
  editedTask,
  searchQuery,
} );
