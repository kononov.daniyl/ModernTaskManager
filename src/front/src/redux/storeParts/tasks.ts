import { _IGenericAction } from '../types';
import { _IStore, AllActions, _IActions } from '.';
import { _ITaskList } from '../../types';
import { select, call, put } from 'redux-saga/effects';
import { getToken, getSearchQuery } from '@Selectors';

declare module './index' {
  interface _IStore {
    tasks: _ITaskList;
  }

  interface _IActionTypes {
    setTasks: 'setTasks';
    createTask: 'createTask';
    deleteTask: 'deleteTask';
    updateTask: 'updateTask';
  }

  interface _IActions {
    setTasks: _IGenericAction<_IActionTypes['setTasks'], _ITaskList >;
    createTask: _IGenericAction<_IActionTypes['createTask'], null>;
    deleteTask: _IGenericAction<_IActionTypes['deleteTask'], null>;
    updateTask: _IGenericAction<_IActionTypes['updateTask'], null>;
  }
}

export const tasks = function tasks( state: _IStore['tasks'] = [] , action: AllActions ) {

  switch( action.type ) {
    case 'setTasks':
      return action.value;
    default:
      return state;
  }
};

export function* fetchTasks() {
  const token: _IStore['token'] = yield select(getToken);
  const searchQuery: _IStore['searchQuery'] = yield select(getSearchQuery);

  const url = searchQuery === '' ? '' : '/find';
  const body = searchQuery === '' ? null : JSON.stringify({query: searchQuery});
  const method = searchQuery === '' ? 'GET' : 'POST'

  try {
     const data = yield call(()=>fetch(`/api/task${url}`, {
       method,
       body,
       headers :{
         Authorization: `Bearer ${token}`,
         'Content-Type': 'application/json'
         }
      }));

      const tasks: _ITaskList = yield data.json();

     yield put<_IActions['setTasks']>({type: 'setTasks', value: tasks});
  } catch (e) {
    console.log('e', e);
  }
}
