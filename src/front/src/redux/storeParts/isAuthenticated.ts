import { _IGenericAction } from '../types';
import { _IStore, AllActions } from '.';

declare module './index' {
  interface _IStore {
    isAuthenticated: boolean;
  }

  interface _IActionTypes {
    setIsAuthenticated: 'setIsAuthenticated';
  }

  interface _IActions {
    setIsAuthenticated: _IGenericAction<_IActionTypes['setIsAuthenticated'], boolean>;
  }
}

export const isAuthenticated = function isAuthenticated( state: _IStore['isAuthenticated'] = false, action: AllActions ) {
  switch( action.type ) {
    case 'setIsAuthenticated':
      return action.value;
    default:
      return state;
  }
};
