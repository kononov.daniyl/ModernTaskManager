import { _IGenericAction } from '../types';
import { _IStore, AllActions } from '.';

declare module './index' {
  interface _IStore {
    token: string;
  }

  interface _IActionTypes {
    setToken: 'setToken';
  }

  interface _IActions {
    setToken: _IGenericAction<_IActionTypes['setToken'], string>;
  }
}

export const token = function token( state: _IStore['token'] = '', action: AllActions ) {
  switch( action.type ) {
    case 'setToken':
      return action.value;
    default:
      return state;
  }
};
