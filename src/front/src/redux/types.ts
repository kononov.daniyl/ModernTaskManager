import { _IActionTypes, AllActions } from './storeParts';

export interface _IGenericAction<T extends keyof _IActionTypes, V> {
  type: T;
  value: V;
}

export const isCertainAction = <
  A extends AllActions,
  ActionType extends AllActions['type']
>(
    action: A,
    type: ActionType,
  ): action is Extract< A, { type: ActionType } > => action.type === type;