import {Schema, model, Types, Document} from 'mongoose';
import { _IMongoUser } from 'src/helpers/types';

type _IUserDocument = _IMongoUser & Document;

const schema = new Schema({
  email: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  tasks:[{type: Types.ObjectId, ref: 'Task'}]
});

const  UserModel = model<_IUserDocument>('User', schema);

export {UserModel};
