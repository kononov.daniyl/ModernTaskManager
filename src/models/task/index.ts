import { Schema, model, Types, Document } from 'mongoose';
import { _ITask } from 'src/front/src/types';

type _ITaskDocument = _ITask & Document;

const schema = new Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  status: { type: String, required: true },
  owner: { type: Types.ObjectId, ref: 'User' }
});

const TaskModel = model<_ITaskDocument>('Task', schema);

export { TaskModel };
