import {Response} from 'express';
import { _IUserAuth } from 'src/front/src/types';

export interface _IMongoTask{
  title: string;
  description: string;
  status: 'done' | 'in progress' | 'expectation' | 'blocked';
}
export interface _IMongoUser{
  email: string;
  password: string;
  tasks: _IMongoTask[]
}

export interface  _IResponseWithUserAuth extends Response {
  locals: Response['locals'] & {
    user: _IUserAuth;
  }
}