import { Response, Request, Router } from "express";
import { TaskModel } from "@Models/task";
import { authMiddlware } from "src/middleware/authMiddlware";
import { _IUserAuth, _ITask, _ITaskWithId, _ITaskList } from "src/front/src/types";
import { _IResponseWithUserAuth } from "src/helpers/types";
import { check, validationResult } from "express-validator";
const router = Router();

router.post(
  '/add',
  [
    check('title')
      .exists().withMessage('Title must be exists')
      .isLength({ min: 3 }).withMessage('Title must be longer than 3 characters')
      .isLength({ max: 30 }).withMessage('Title must be shorter than 30 characters'),
    check('status')
      .exists().withMessage('Status must be exists'),
    check('description')
      .exists().withMessage('Description must be exists')
      .isLength({ min: 3 }).withMessage('Description must be longer than 3 characters')
      .isLength({ max: 300 }).withMessage('Description must be shorter than 300 characters'),
  ],
  authMiddlware,
  async (req: Request, res: Response) => {
    const typedResponse = res as _IResponseWithUserAuth;

    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: 'Incorrect data'
        });
      }

      const {
        description,
        status,
        title
      }: _ITask = req.body;

      const task = new TaskModel({
        description, status, title, owner: typedResponse.locals.user.userId
      });

      await task.save();

      typedResponse.status(201).json({ task });

    } catch (e) {
      return res.status(500).json({ message: 'Something went wrong. try later' });
    }
  }
);

router.delete(
  '/delete',
  authMiddlware,
  async (req: Request, res: Response) => {

    try {
      const {
        _id
      }: { _id: _ITaskWithId['_id'] } = req.body;

      const maybeTask = await TaskModel.findOne({ _id });

      if (!maybeTask) {
        return res.status(404).json({ message: 'Task not found' });
      }

      await TaskModel.deleteOne({ _id });

      res.json({ message: `Task ${maybeTask.title} deleted successfully` });

    } catch (e) {
      return res.status(500).json({ message: 'Something went wrong. try later' });
    }
  }
);

router.patch(
  '/edit',
  authMiddlware,
  async (req: Request, res: Response) => {
    try {
      const newTask: _ITaskWithId = req.body;

      const maybeTask = await TaskModel.findOne({ _id: newTask._id });

      if (!maybeTask) {
        return res.status(404).json({ message: 'Task not found' });
      }

      await TaskModel.updateOne({ _id: maybeTask._id }, newTask);

      res.json({ message: `Task ${maybeTask.title} updated successfully` });

    } catch (e) {
      return res.status(500).json({ message: 'Something went wrong. try later' });
    }
  }
);

router.get(
  '/',
  authMiddlware,
  async (req: Request, res: Response) => {
    const typedResponse = res as _IResponseWithUserAuth;

    try {
      const tasks = await TaskModel.find({ owner: typedResponse.locals.user.userId });

      typedResponse.json(tasks);
    } catch (e) {
      return typedResponse.status(500).json({ message: 'Something went wrong. try later' });
    }
  }
);

router.post(
  '/find',
  authMiddlware,
  async (req: Request, res: Response) => {
    const typedResponse = res as _IResponseWithUserAuth;
    try {
      const {
        query
      }: { query: string } = req.body;

      const tasks = await TaskModel.find({
        $and: [
          { owner: typedResponse.locals.user.userId },
          { title: { $regex: query } }
        ]
      });

      typedResponse.json(tasks);
    } catch (e) {
      return typedResponse.status(500).json({ message: 'Something went wrong. try later' });
    }
  }
);

router.get(
  '/:id',
  authMiddlware,
  async (req: Request, res: Response) => {
    try {
      const task = await TaskModel.findById(req.params.id);

      res.json(task);
    } catch (e) {
      return res.status(500).json({ message: 'Something went wrong. try later' });
    }
  }
);



export { router as taskRouter };