import { Router, Response, Request } from 'express';
import { _IMongoUser } from 'src/helpers/types';
import { UserModel } from '@Models/user';
import bcrypt from 'bcrypt';
import { check, validationResult } from 'express-validator';
import jwt from 'jsonwebtoken';
import { JWT_SRCRET_KEY } from '@Env';

const router = Router();

router.post(
  '/register',
  [
    check('email', 'Incorrect email').isEmail(),
    check('password')
      .isLength({ min: 6 }).withMessage('Password is shorter than 6')
      .isLength({ max: 20 }).withMessage('Password is longer than 20')
      .custom((value, {req}) => value === req.body.confirmPassword).withMessage("Passwords don't match."),
    check('confirmPassword')
      .isLength({ min: 6 }).withMessage('ConfirmPassword is shorter than 6')
      .isLength({ max: 20 }).withMessage('ConfirmPassword is longer than 20'),
  ],
  async (req: Request, res: Response, ) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: 'Incorrect data'
        });
      }

      const {
        email,
        password,
      }: _IMongoUser = req.body;

      const maybeUser = await UserModel.findOne({ email });

      if (maybeUser !== null) {
        return res.status(400).json({ message: 'User already exists' });
      }

      const hashedPassword = await bcrypt.hash(password, 10);
      const newUser = new UserModel({ email, password: hashedPassword });

      await newUser.save();

      return res.status(201).json({ message: 'User created' });
    } catch (e) {
      return res.status(500).json({ message: 'Something went wrong. try later' });
    }
  })

router.post(
  '/login',
  [
    check('email', 'Wrong email').isEmail(),
    check('password').exists()
  ],
  async (req: Request, res: Response) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: 'Incorrect data'
        });
      }

      const {
        email,
        password
      }: _IMongoUser = req.body;

      const maybeUser = await UserModel.findOne({ email });

      if(!maybeUser){
        return res.status(400).json({message: 'Email or password is wrong'});
      }

      const isDataMatch = await bcrypt.compare(password, maybeUser.password);

      if(!isDataMatch){
        return res.status(400).json({message: 'Email or password is wrong'});
      }

      const token = jwt.sign(
        {userId: maybeUser.id},
        JWT_SRCRET_KEY,
        {expiresIn: '1d'}
      );

      res.json({token, userId: maybeUser.id});

    } catch (e) {
      return res.status(500).json({ message: 'Something went wrong. try later' });
    }
  })

export { router as authRouter };