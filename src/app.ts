import express from 'express';
import mongoose from 'mongoose';
import config from '@Config';
import { authRouter } from '@Routes/auth';
import { taskRouter } from '@Routes/task';

const app = express();

app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/task', taskRouter);

(async ()=>{
  try{
    await mongoose.connect(config.mongoUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    app.listen(config.PORT, ()=>console.log(`port: ${config.PORT}`));


  } catch(e){
    console.log('error', e);
    process.exit(1);
  }
})();