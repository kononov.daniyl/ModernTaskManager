import { Response, Request, NextFunction } from "express";
import jwt from 'jsonwebtoken';
import { JWT_SRCRET_KEY } from "@Env";

export const authMiddlware = (req: Request, res: Response, next: NextFunction) => {
  if(req.method === 'OPTIONS'){
    return next();
  }

  try{
    const token = req.headers.authorization ? req.headers.authorization.split(' ')[1] : null;
    if(!token){
      return res.status(401).json({message: 'Not authenticated'});
    }

    const decodedUser = jwt.verify(token, JWT_SRCRET_KEY);

    res.locals.user = decodedUser;
    next();
  } catch(e){
    res.status(401).json({message: 'Not authenticated'});
  }
}