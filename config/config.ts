import { MONGO_URI_DB_NAME, MONGO_URI_PASSWORD } from "@Env";

export default {
  PORT: 5000,
  mongoUri: `mongodb+srv://kononov:${MONGO_URI_PASSWORD}@cluster0.xbohi.mongodb.net/${MONGO_URI_DB_NAME}?retryWrites=true&w=majority`
} as const;