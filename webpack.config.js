const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  watch: true,
  devtool: 'inline-sourcemap',
  entry: "./src/front/src/index.tsx",
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    port: 3000,
    hot: true,
    proxy: {
      '/api': 'http://localhost:5000'
    },
    historyApiFallback: true

  },
  output: {
    path: path.join(__dirname, "build"),
    filename: "index.js",
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    alias: {
      src: path.resolve( __dirname, 'src' ),
      '@Components': path.resolve( __dirname, 'src', 'front','src', 'components' ),
      '@Pages': path.resolve(__dirname, 'src', 'front', 'src', 'pages'),
      '@Routes': path.resolve(__dirname, 'src', 'routes'),
      '@Models': path.resolve(__dirname, 'src', 'models'),
      '@Hooks': path.resolve(__dirname, 'src', 'front', 'src', 'hooks'),
      '@Redux': path.resolve(__dirname, 'src', 'front', 'src', 'redux'),
      '@Images': path.resolve(__dirname, 'public', 'img'),
      '@Router': path.resolve(__dirname, 'src', 'front', 'src', 'Router.tsx'),
      '@Theme': path.resolve( __dirname, 'src', 'front', 'Theme.ts', ),
      '@Config': path.resolve( __dirname, 'config', 'Config.ts', ),
      '@Env': path.resolve(__dirname, 'src', 'helpers', 'EnvVariables.ts'),
      '@Selectors': path.resolve(__dirname, 'src', 'front', 'src', 'Selectors.ts'),
    }
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              compact: true,
              cacheDirectory: true,
              presets: [
                '@babel/preset-env',
                '@babel/preset-react',
                '@babel/preset-typescript'
              ],
              plugins: [
                '@babel/plugin-proposal-class-properties'
              ],
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        use: [
          'svg-react-loader',
        ]
      },
    ],
  },
  plugins:[
    new HtmlWebpackPlugin({
       template: './public/index.html'
    })
 ]
};
